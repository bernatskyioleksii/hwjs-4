
//#1
// Функції потрібні щоб не повторювати той самий код у багатьох місцях.

//#2
// Аргумент – це значення, яке передається функції при її виклику.
// приклад     function mathematicalOperations-функція, (a, b, operator)-параметри
// приклад     mathematicalOperations-функція, (a, b, inputOperator)-Аргумент

//#3
// Оператор "return" може повернути результат.

// -------------------------------------------------

let userFirstNumber = +prompt("Enter the first number 'allowed 0.1'");
while (userFirstNumber === 0  || isNaN(userFirstNumber)) {
    userFirstNumber = +prompt("correct number ");
}
let userSecondNumber = +prompt("Enter the second number 'allowed 0.1'");
while (userSecondNumber === 0  || isNaN(userSecondNumber)) {
    userSecondNumber = +prompt("correct number ");
}

let inputOperator = prompt("Enter operator: + , - , * , / ");
// const operator = "+ , - , * , /";

while(inputOperator !== "+" && inputOperator !== "-" && inputOperator !== "*" && inputOperator !== "/")
    inputOperator = prompt("Enter correct operator: + , - , * , / ");


function mathematicalOperations(a, b, operator) {

    if (operator === "+") {
        return (`Addition: ${a} + ${b} = ${a+b}`);
    }else if (operator === "-") {
        return (`Addition: ${a} - ${b} = ${a-b}`);
    }else if (operator === "*") {
		 return(`Multiplication: ${a} * ${b} = ${a*b}`);
    }else if (operator === "/") {
	    return (`Division: ${a} / ${b} = ${a/b}`);
    }


    switch (operator) {
        case "+":
            return (`Addition: ${a} + ${b} = ${a+b}`);

        case "-":
            return(`Subtraction: ${a} - ${b} = ${a-b}`);

        case "*":
           return(`Multiplication: ${a} * ${b} = ${a*b}`);

        case "/":
            return(`Division: ${a} / ${b} = ${a/b}`);

        default:
    }
}

console.log(mathematicalOperations(userFirstNumber, userSecondNumber, inputOperator));

// --------------------------------------------------------------------------------------
  //#1
//     	if (operator === "+") {
// 		 console.log(`Addition: ${a} + ${b} = ${a+b}`);
// 	}else if (operator === "-") {
// 		 console.log(`Subtraction: ${a} - ${b} = ${a-b}`);
// 	}else if (operator === "*") {
// 		 console.log(`Multiplication: ${a} * ${b} = ${a*b}`);
//     }else if (operator === "/") {
// 	     console.log(`Division: ${a} / ${b} = ${a/b}`);
//     }

// #2
// switch (operator) {
//     case "+":
//         console.log(`Addition: ${a} + ${b} = ${a+b}`);
//         break;
//     case "-":
//         console.log(`Subtraction: ${a} - ${b} = ${a-b}`);
//         break;
//     case "*":
//         console.log(`Multiplication: ${a} * ${b} = ${a*b}`);
//         break;
//     case "/":
//         console.log(`Division: ${a} / ${b} = ${a/b}`);
//         break;
//     default:
//
// }
// mathematicalOperations(userFirstNumber, userSecondNumber, inputOperator);




